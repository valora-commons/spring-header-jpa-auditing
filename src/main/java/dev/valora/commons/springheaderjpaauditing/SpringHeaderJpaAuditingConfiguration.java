package dev.valora.commons.springheaderjpaauditing;

import java.time.OffsetDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Spring configuration to import to use Spring header JPA auditing.
 */
@Configuration
@EnableJpaAuditing(dateTimeProviderRef = "auditingDateTimeProvider", auditorAwareRef = "auditorProvider", modifyOnCreate = false)
public class SpringHeaderJpaAuditingConfiguration {

	@Value("${user.id.header.name:User-Id}")
	private String userIdHeaderName;

	@Bean(name = "auditingDateTimeProvider")
	public DateTimeProvider dateTimeProvider() {
		return () -> Optional.of(OffsetDateTime.now());
	}

	@Bean(name = "auditorProvider")
	public AuditorAware<String> auditorProvider() {
		return () -> Optional.ofNullable(RequestContextHolder.getRequestAttributes())
				.filter(requestAttributes -> ServletRequestAttributes.class.isAssignableFrom(requestAttributes.getClass()))
				.map(ServletRequestAttributes.class::cast).map(ServletRequestAttributes::getRequest)
				.map(request -> request.getHeader(userIdHeaderName));
	}

}
