package dev.valora.commons.springheaderjpaauditing.model;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Superclass to extend to have JPA auditing field in subclass entity.
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditingEntity {

	@CreatedDate
	@Column(name = "creation_date_time")
	private ZonedDateTime creationDateTime;

	@LastModifiedDate
	@Column(name = "last_modification_date_time")
	private ZonedDateTime lastModificationDateTime;

	@CreatedBy
	@Column(name = "creation_user_id")
	private String creationUserId;

	@LastModifiedBy
	@Column(name = "last_modification_user_id")
	private String lastModificationUserId;

	public ZonedDateTime getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(ZonedDateTime creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public ZonedDateTime getLastModificationDateTime() {
		return lastModificationDateTime;
	}

	public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
		this.lastModificationDateTime = lastModificationDateTime;
	}

	public String getCreationUserId() {
		return creationUserId;
	}

	public void setCreationUserId(String creationUserId) {
		this.creationUserId = creationUserId;
	}

	public String getLastModificationUserId() {
		return lastModificationUserId;
	}

	public void setLastModificationUserId(String lastModificationUserId) {
		this.lastModificationUserId = lastModificationUserId;
	}

}
