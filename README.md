# Spring header JPA auditing

Add Spring request header user ID to JPA auditing (creation date time, last modification date time, creation user id, last modification user id).

## Usage

### Maven

Import the latest `dev.valora.commons:spring-header-jpa-auditing` artifact into your pom.xml.

```
<dependency>
	<groupId>dev.valora.commons</groupId>
	<artifactId>spring-header-jpa-auditing</artifactId>
	<version>2.0.0</version>
</dependency>
```

### Entity

Extends `dev.valora.commons.springheaderjpaauditing.model.AuditingEntity` with your JPA entities that requires auditing.

### Spring configuration

Import `dev.valora.commons.springheaderjpaauditing.SpringHeaderJpaAuditingConfiguration` to your Spring configuration.

```
@Import(SpringHeaderJpaAuditingConfiguration.class)
```

Add `user.id.header.name` to your Spring configuration properties to set an other header name than `User-Id`.

## Requirements

* Java 11
* Spring Boot 2.4.5